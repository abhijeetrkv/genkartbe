package com.generate.certificate.genkartBE.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.zeroturnaround.zip.ZipUtil;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.generate.certificate.genkartBE.Constants;
import com.generate.certificate.genkartBE.Utility;
import com.generate.certificate.genkartBE.entity.Candidate;
import com.generate.certificate.genkartBE.entity.CandidateList;
import com.generate.certificate.genkartBE.entity.Report;
import com.generate.certificate.genkartBE.entity.Template;
import com.generate.certificate.genkartBE.entity.User;
import com.generate.certificate.genkartBE.entity.UserDisplayModel;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRXmlUtils;

@CrossOrigin(origins= {"http://localhost:4200","http://103.190.243.5:8080"})
@RestController
public class GenerateCertificateController {
	
	@Value("${api.report.save}")
	private String saveReportUrl;

	@Value("${api.key}")
	private String apiKey;
	
	static File file = null;
	static File zipfile = null;
	static File logoImage = null;
	static File signImage = null;
	static List<Candidate> failedCandidateList = new ArrayList<Candidate>();
	
	static int progressPercentage = 0;
	
	private static final String DATE_PATTERN = "yyyy-MM-dd";
	private static final String NUMBER_PATTERN = "#,##0.##";
	private Map<String, Integer> certificateMap = new LinkedHashMap<String, Integer>();
	
	private final Path fileStorageLocation = null;

	@PostMapping("/generateCertificate/{templateId}/{userId}")
	public String handleGenerateCertificate(
			@RequestParam("file") MultipartFile mulfile,
			@RequestParam("zipfile") MultipartFile mulzipfile,
			@RequestParam("logoImage") MultipartFile logoImageFile,
			@RequestParam("signImage") MultipartFile signImageFile,
			HttpServletRequest request, @PathVariable String templateId,@PathVariable Integer userId) throws IOException {
		try {
			failedCandidateList =new ArrayList<Candidate>();
			progressPercentage = 10;
	        System.out.println( "-----------1---START-----"+new java.util.Date());
	        
			file = new File(System.getProperty("java.io.tmpdir")+"/"+mulfile.getOriginalFilename());
			// Write bytes from the multipart file to disk.
			FileUtils.writeByteArrayToFile(file, mulfile.getBytes());
			
			if(!"null".equals(mulzipfile.getOriginalFilename())) {
				/***********************ZIP FILE CODE************************/
				zipfile = new File(System.getProperty("catalina.base")+
						"/files/output/"+mulzipfile.getOriginalFilename());
				FileUtils.writeByteArrayToFile(zipfile, mulzipfile.getBytes());
				
				ZipUtil.unpack(new File(System.getProperty("catalina.base")+"/files/output/"+mulzipfile.getOriginalFilename())
						,new File(System.getProperty("catalina.base")+"/files/output/"));
			}
			
			if(!"null".equals(logoImageFile.getOriginalFilename())) {
				/***********************LOGO IMAGE************************/
				logoImage = new File(System.getProperty("catalina.base")+
						"/files/output/logoSignImages/"+logoImageFile.getOriginalFilename());
				FileUtils.writeByteArrayToFile(logoImage, logoImageFile.getBytes());
				
			}
			
			if(!"null".equals(signImageFile.getOriginalFilename())) {
				/***********************SIGN IMAGE************************/
				signImage = new File(System.getProperty("catalina.base")+
						"/files/output/logoSignImages/"+signImageFile.getOriginalFilename());
				FileUtils.writeByteArrayToFile(signImage, signImageFile.getBytes());
	
			}
			
			System.out.println( "-----------2---"+new java.util.Date());
			String path = processFile(templateId,userId);
			
			System.out.println( "-----------14---DONE-----"+new java.util.Date());
			return "\"" + path.replace("\\", "/") + "\"";
		}catch(Exception e) {
			e.printStackTrace();
			return "Certificate Generation Error";
		}
		
	}

	private static String processFile(String certificateType, int loggedInUserId) throws IOException {

		List<Candidate> candidates = Utility.getCandidatesFromFile(file);
		if(candidates.size()>0) {
			progressPercentage = 15;
			System.out.println( "-----------3---"+new java.util.Date());
			CandidateList candidateList = new CandidateList();
			
			if(zipfile != null) {
				progressPercentage = 35;
				for(Candidate candidate : candidates) {
					List<Candidate> uniCandidateList = new ArrayList<Candidate>();
					uniCandidateList.add(candidate);
					candidateList.setCandidates(uniCandidateList);
					String xml = Utility.convertObjectToXML(candidateList, CandidateList.class);
					System.out.println( "-----------4---"+new java.util.Date());
					// PDF Generation from XML
					// System.out.println("XML : "+xml);
					Integer certType = Integer.parseInt(certificateType);
					try {
						String pdfPath = generateImageEnabledCertificates(xml, certType, candidate);
						saveImagePDFs(pdfPath, candidate, certType, loggedInUserId);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					if(progressPercentage<=90)
					progressPercentage +=5;
				}
				progressPercentage = 90;
				System.out.println( "-----------13---"+new java.util.Date());
				String folderPath = System.getProperty("catalina.base") + "/Candidate Image Certificates";
				
				writeFailedCandidateList();
				
				ZipUtil.pack(new File(folderPath)
						,new File(System.getProperty("catalina.base")+"/Certificates.zip"));
				progressPercentage = 100;
				FileUtils.deleteDirectory(new File(folderPath));
				System.out.println( "-----------14---"+new java.util.Date());
				return folderPath;
			}else {
				candidateList.setCandidates(candidates);
				String xml = Utility.convertObjectToXML(candidateList, CandidateList.class);
				System.out.println( "-----------4---"+new java.util.Date());
				// PDF Generation from XML
				// System.out.println("XML : "+xml);
				Integer certType = Integer.parseInt(certificateType);
				String pdfPath = generateCertificateXML(xml, certType);
				try {
					String path = splitandSavePDFs(pdfPath, candidates, certType, loggedInUserId);
					return path;
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}else {
			return "Candidate Data not found, Please check Sheet name.";
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static String generateCertificateXML(String xml, int templateId) throws IOException {
		System.out.println( "-----------5---"+new java.util.Date());
		Map params = new HashMap();
		Document document;
		
		String inputfolderPath = System.getProperty("catalina.base")+"/files/";
		String outputfolderPath = System.getProperty("catalina.base")+"/files/output/";
		String outputfolderlogoSignImgPath = System.getProperty("catalina.base")+"/files/output/logoSignImages/";
		
		try {
			InputStream xmlStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_16));
			document = JRXmlUtils.parse(xmlStream);
			params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
			params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, DATE_PATTERN);
			params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, NUMBER_PATTERN);
			params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);
			params.put(JRParameter.REPORT_LOCALE, Locale.US);
			params.put("imgParam1", inputfolderPath + "/commonIMG/img1.png");
			params.put("imgParam2", inputfolderPath + "/commonIMG/img2.png");
			params.put("imgParam3", inputfolderPath + "/commonIMG/img3.png");
			params.put("imgParam4", inputfolderPath + "/template"+templateId+"/img4.png");
			params.put("imgParam5", inputfolderPath + "/template"+templateId+"/img5.png");
			params.put("imgParam6", inputfolderPath + "/template"+templateId+"/img6.png");
			params.put("imgParam7", inputfolderPath + "/template"+templateId+"/img7.png");
			params.put("imgParam8", inputfolderPath + "/template"+templateId+"/img8.png");
			params.put("imgParam9", inputfolderPath + "/template"+templateId+"/img9.png");
			params.put("imgParam12", inputfolderPath + "/template"+templateId+"/img12.png");
			params.put("imgParam13", inputfolderPath + "/template"+templateId+"/img13.png");
			params.put("imgParam14", inputfolderPath + "/template"+templateId+"/img14.png");
			if (templateId == 11) {
				params.put("imgParam8", inputfolderPath + "/template"+templateId+"/img8.png");
			}
			
			if(logoImage != null) {
				params.put("imgParam10", outputfolderlogoSignImgPath + logoImage.getName());
			}
			
			if(signImage !=null) {
				params.put("imgParam11", outputfolderlogoSignImgPath + signImage.getName());
			}
			params.put("imgParamBG", inputfolderPath +"/template"+templateId+"/jasperBG.png");
			System.out.println( "-----------6---"+new java.util.Date());
			JasperCompileManager.compileReportToFile(inputfolderPath + "/template"+templateId+"/XMLCertificateSS" + templateId + ".jrxml",
					outputfolderPath + "XMLCertificate.jasper");
			progressPercentage = 20;
			System.out.println( "-----------7---"+new java.util.Date());
			JasperFillManager.fillReportToFile(outputfolderPath + "XMLCertificate.jasper", params);
			progressPercentage = 35;
			System.out.println( "-----------8---"+new java.util.Date());
			JasperExportManager.exportReportToPdfFile(outputfolderPath + "XMLCertificate.jrprint",
					outputfolderPath + "XMLCertificate.pdf");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputfolderPath + "XMLCertificate.pdf";
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static String generateImageEnabledCertificates(String xml, int templateId, Candidate candidate) throws IOException {
		System.out.println( "-----------5---"+new java.util.Date());
		Map params = new HashMap();
		Document document;
		
		String inputfolderPath = System.getProperty("catalina.base")+"/files/";
		String outputfolderPath = System.getProperty("catalina.base")+"/files/output/";
		
		try {
			InputStream xmlStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_16));
			document = JRXmlUtils.parse(xmlStream);
			params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
			params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, DATE_PATTERN);
			params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, NUMBER_PATTERN);
			params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);
			params.put(JRParameter.REPORT_LOCALE, Locale.US);
			
			params.put("imgParam4", outputfolderPath+ "/Images/"+candidate.getSid()+".png");
			
			params.put("imgParam5", inputfolderPath + "/template"+templateId+"/img5.png");
			params.put("imgParam6", inputfolderPath + "/template"+templateId+"/img6.png");
			params.put("imgParam7", inputfolderPath + "/template"+templateId+"/img7.png");
			params.put("imgParam8", inputfolderPath + "/template"+templateId+"/img8.png");
			params.put("imgParam9", inputfolderPath + "/template"+templateId+"/img9.png");
			
			params.put("imgParamBG", inputfolderPath +"/template"+templateId+"/jasperBG.png");
			
			String folderPath = System.getProperty("catalina.base") + "/Candidate Image Certificates/";
			System.out.println("Making directory at " + folderPath);
			File dir = new File(folderPath);// The name of the directory to create
			dir.mkdirs();// Creates the directory
			
			System.out.println( "-----------6---"+new java.util.Date());
			JasperCompileManager.compileReportToFile(inputfolderPath + "/template"+templateId+"/XMLCertificateSS" + templateId + ".jrxml",
					outputfolderPath + "XMLCertificate.jasper");
			System.out.println( "-----------7---"+new java.util.Date());
			JasperFillManager.fillReportToFile(outputfolderPath + "XMLCertificate.jasper", params);
			System.out.println( "-----------8---"+new java.util.Date());
			JasperExportManager.exportReportToPdfFile(outputfolderPath + "XMLCertificate.jrprint",
					folderPath + candidate.getSid() + "_" + candidate.getCandidateName() + ".pdf");

		} catch (JRException e) {
			failedCandidateList.add(candidate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		zipfile = null;
		return outputfolderPath + "XMLCertificate.pdf";
	}

	protected static String splitandSavePDFs(String pdfPath, List<Candidate> candidates, Integer certType
			,int loggedInUserId) throws IOException {
		progressPercentage = 50;
		System.out.println( "-----------9---"+new java.util.Date());
		// Loading an existing PDF document
		File file = new File(pdfPath);
		PDDocument doc = PDDocument.load(file);
		progressPercentage = 65;
		System.out.println( "-----------10---"+new java.util.Date());
		// Instantiating Splitter class
		Splitter splitter = new Splitter();

		// splitting the pages of a PDF document
		List<PDDocument> Pages = splitter.split(doc);
		System.out.println( "-----------11---"+new java.util.Date());
		// Creating an iterator
		Iterator<PDDocument> iterator = Pages.listIterator();

		String folderPath = System.getProperty("catalina.base") + "/Certificates/";
		String scoreCardPath = System.getProperty("catalina.base") + "/ScoreCards/";
		System.out.println("Making directory at " + folderPath);
		File dir = new File(folderPath);// The name of the directory to create
		dir.mkdirs();// Creates the directory

		// Saving each page as an individual document
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		int i = 0;
		List<Report> list = new ArrayList<Report>();
		while (iterator.hasNext()) {
			String path1 = folderPath + candidates.get(i).getSid() + "_" + candidates.get(i).getCandidateName()
					+ ".pdf";
			String path2 = scoreCardPath + candidates.get(i).getSid() + "_" + candidates.get(i).getCandidateName()
					+ ".pdf";
			Report report = new Report();
			report.setSid(candidates.get(i).getSid());
			report.setGeneratedBy(String.valueOf(loggedInUserId));
			report.setGeneratedOn(new Date());
			report.setCandidateName(candidates.get(i).getCandidateName());
			report.setCourseName(candidates.get(i).getCourseName());
			report.setLevel(candidates.get(i).getLevel());
			report.setTemplateID(certType.longValue());
			report.setTrainingPartner(candidates.get(i).getColumn2());
			report.setBatchId(candidates.get(i).getColumn3());
			list.add(report);
			PDDocument pd = iterator.next();
			pd.save(folderPath + candidates.get(i).getSid() + "_" + candidates.get(i).getCandidateName() + ".pdf");

			if (certType == 6 || certType == 11) {
				mergeScoreCardwithPDF(path1, path2);
			}
			i++;
		}
		doc.close();
		progressPercentage = 75;
		System.out.println( "-----------12---"+new java.util.Date());
		Utility.postData(Constants.API_SAVE_REPORT, new ObjectMapper().writeValueAsString(list));
		progressPercentage = 90;
		System.out.println( "-----------13---"+new java.util.Date());
		ZipUtil.pack(new File(System.getProperty("catalina.base")+"/Certificates")
				,new File(System.getProperty("catalina.base")+"/Certificates.zip"));
		progressPercentage = 100;
		System.out.println( "-----------14---"+new java.util.Date());
		FileUtils.deleteDirectory(new File(System.getProperty("catalina.base")+"/Certificates"));
		
		System.out.println( "-----------15---"+new java.util.Date());
		return folderPath;

	}
	
	protected static String saveImagePDFs(String pdfPath, Candidate candidate, Integer certType,
			int loggedInUserId) throws IOException {
		System.out.println( "---9---"+new java.util.Date());
		System.out.println( "---10---"+new java.util.Date());

		String folderPath = System.getProperty("catalina.base")+ "/Candidate Image Certificates";
		List<Report> list = new ArrayList<Report>();

		Report report = new Report();
		report.setSid(candidate.getSid());
		report.setGeneratedBy(String.valueOf(loggedInUserId));
		report.setGeneratedOn(new Date());
		report.setCandidateName(candidate.getCandidateName());
		report.setCourseName(candidate.getCourseName());
		report.setLevel(candidate.getLevel());
		report.setTemplateID(certType.longValue());
		list.add(report);

		System.out.println( "-----------12---"+new java.util.Date());
		Utility.postData(Constants.API_SAVE_REPORT, new ObjectMapper().writeValueAsString(list));
		return folderPath;
	}
	
	@SuppressWarnings("deprecation")
	protected static void mergeScoreCardwithPDF(String path1, String path2) throws IOException {
		 //Loading an existing PDF document 

	      File file2 = new File(path2); 
	      PDDocument doc2 = PDDocument.load(file2); 
	      
	      File file1 = new File(path1); 
	      PDDocument doc1 = PDDocument.load(file1); 

	      //Instantiating PDFMergerUtility class 
	      PDFMergerUtility PDFmerger = new PDFMergerUtility();       

	      //Setting the destination file 
	      PDFmerger.setDestinationFileName(path1); 

	      //adding the source files 
	      PDFmerger.addSource(file1); 
	      PDFmerger.addSource(file2); 

	      //Merging the two documents 
	      PDFmerger.mergeDocuments(); 
	      System.out.println("Documents merged"); 

	      //Closing the documents 
	      doc1.close(); 
	      doc2.close();  
		
	}
	
	public Resource loadFileAsResource(String fileName) throws Exception {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new Exception("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new Exception("File not found " + fileName, ex);
        }
    }
	
	@GetMapping("/downloadCertificateZip")
	public ResponseEntity<byte[]> downloadCertificateZip() throws Exception {
		try {
			String path = System.getProperty("catalina.base")+"/Certificates.zip";
			byte[] isr = Files.readAllBytes(Paths.get(path));

			String fileName = "Certificates.zip";

			HttpHeaders respHeaders = new HttpHeaders();
			respHeaders.setContentLength(isr.length);
			//respHeaders.setContentType(new MediaType("text", "json"));
			respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);

			return new ResponseEntity<byte[]>(isr, respHeaders, HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<byte[]>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping("/templates/{userId}")
	public Template[] getAssignedTemplates(@PathVariable Integer userId) throws Exception{
		Template[] templateList=null;
		try {
			String url = Constants.API_TEMPLATE+"?userId="+userId;
			ResponseEntity<Template[]> responseEntity = new RestTemplate().getForEntity(
					url, Template[].class);
			if(responseEntity.getStatusCode() == HttpStatus.OK) {
				templateList = responseEntity.getBody();
			}else {
				throw new Exception(responseEntity.getStatusCode().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return templateList;
	}
	
	@GetMapping("/loader/getProgressBarPercentage")
	public Integer getFilteredReports() throws Exception{
		return progressPercentage;
	}
	
	private static void writeFailedCandidateList() {
		progressPercentage =95;
		try {
			String filepath = System.getProperty("catalina.base") + "/Candidate Image Certificates/Failed Candidate List.txt";
			File errorlog = new File(filepath);
			errorlog.createNewFile();
			
			FileWriter writer = new FileWriter(filepath);
			for(Candidate candidate : failedCandidateList) {
				writer.write(candidate.getSid()+" - "+candidate.getCandidateName());
				writer.write(System.lineSeparator());
			}
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
