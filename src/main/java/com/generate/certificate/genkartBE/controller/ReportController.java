package com.generate.certificate.genkartBE.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.generate.certificate.genkartBE.Constants;
import com.generate.certificate.genkartBE.entity.Report;
import com.generate.certificate.genkartBE.entity.User;
import com.generate.certificate.genkartBE.entity.UserDisplayModel;

@CrossOrigin(origins= {"http://localhost:4200","http://103.190.242.205:8080"})
@RestController
public class ReportController {
	
	@GetMapping("/filteredReports/{userId}/{fromDate}/{toDate}")
	public Report[] getFilteredReports(@PathVariable String fromDate, @PathVariable String toDate
			,@PathVariable Integer userId) throws Exception{
		Report[] filteredReportList=null;

		try {
			String url = Constants.API_FILTER_REPORT+"?fromDate="+fromDate+"&toDate="+toDate+"&userId="+userId;
			ResponseEntity<Report[]> responseEntity = new RestTemplate().getForEntity(
					url, Report[].class);
			if(responseEntity.getStatusCode() == HttpStatus.OK) {
				filteredReportList = responseEntity.getBody();
			}else {
				throw new Exception(responseEntity.getStatusCode().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filteredReportList;
	}

}
