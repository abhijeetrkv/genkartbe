package com.generate.certificate.genkartBE.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.generate.certificate.genkartBE.Constants;
import com.generate.certificate.genkartBE.entity.Template;
import com.generate.certificate.genkartBE.entity.User;
import com.generate.certificate.genkartBE.entity.UserDisplayModel;

@CrossOrigin(origins= {"http://localhost:4200","http://103.190.242.205:8080"})
@RestController
@Deprecated
public class LoginController {
	
	@Autowired
	ObjectFactory<HttpSession> httpSessionFactory;
	
	//public static User loggedInuser = null;
	
	//AB
	@Deprecated
	@PostMapping("/users/login")
	
	/**
	 * USING BASICAUTHENTICATIONCONTROLLER instead of this one for login
	 */
	public User getUser(@RequestBody User user) throws Exception{
		String email = user.getUserName();
		String pass= user.getPassword();
		HttpSession session = httpSessionFactory.getObject();
		try {
			String url = Constants.AUTH_API_URL+"?email="+email+"&password="+pass;
			ResponseEntity<User> responseEntity = new RestTemplate().getForEntity(
					url, User.class);
			if(responseEntity.getStatusCode() == HttpStatus.OK) {
				user = responseEntity.getBody();
				if(email.equals(user.getEmail()) && pass.contentEquals(user.getPassword())) {
					//loggedInuser = user;
					return user;
				}
			}else {
				throw new Exception("Bad Credentials");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Bad Credentials");
		}
		return null;
	}
	
	/*
	 * @GetMapping("/getLoggedInUser") public UserDisplayModel getLoggedInUser()
	 * throws Exception{ try { return loggedInuser; } catch (Exception e) {
	 * e.printStackTrace(); throw new Exception("Bad Credentials"); } }
	 */
	
	@PostMapping("/users/register")
	public User addUser(@RequestBody User user) throws Exception{
		HttpSession session = httpSessionFactory.getObject();
		try {
			HttpEntity<User> requestEntity = new HttpEntity<>(user);
			String url = Constants.API_REGISTER;
			ResponseEntity<User> responseEntity = new RestTemplate().
					exchange(url, HttpMethod.POST, requestEntity, User.class);
					
					
			if(responseEntity.getStatusCode() == HttpStatus.OK) {
				user = responseEntity.getBody();
				System.out.println("User Created");
				return user;
			}else {
				throw new Exception("Error in creating User");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in creating User");
		}
	}
	
	

}
