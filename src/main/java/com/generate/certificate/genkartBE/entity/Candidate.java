package com.generate.certificate.genkartBE.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Candidate {
	private String salutation;
	private String candidateName;
	private String sid;
	private String courseName;
	private String guardianType;
	private String fatherORHusbandName;
	private String sectorSkillCouncil;
	private String dateOfIssuance;
	private String level;
	private String aadhaarNumber;
	private String sector;
	private String grade;
	private String dateOfStart;
	private String district;
	private String state;
	private String duration;
	private String dateColumn1;
	private String column2;
	private String column3;
	private String column4;
	private String column5;
	private String column6;
	private String column7;
	private String column8;
	private String column9;
	private String column10;
	
	
	@XmlElement
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	@XmlElement  
	public String getCandidateName() {
		return candidateName;
	}
	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}
	@XmlElement  
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	@XmlElement  
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	@XmlElement  
	public String getGuardianType() {
		return guardianType;
	}
	public void setGuardianType(String guardianType) {
		this.guardianType = guardianType;
	}
	@XmlElement  
	public String getFatherORHusbandName() {
		return fatherORHusbandName;
	}
	public void setFatherORHusbandName(String fatherORHusbandName) {
		this.fatherORHusbandName = fatherORHusbandName;
	}
	@XmlElement  
	public String getSectorSkillCouncil() {
		return sectorSkillCouncil;
	}
	public void setSectorSkillCouncil(String sectorSkillCouncil) {
		this.sectorSkillCouncil = sectorSkillCouncil;
	}
	@XmlElement  
	public String getDateOfIssuance() {
		return dateOfIssuance;
	}
	public void setDateOfIssuance(String dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}
	@XmlElement  
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	@XmlElement  
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}
	
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	@XmlElement  
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	@XmlElement
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	@XmlElement
	public String getDateOfStart() {
		return dateOfStart;
	}
	public void setDateOfStart(String dateOfStart) {
		this.dateOfStart = dateOfStart;
	}
	@XmlElement
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	@XmlElement
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@XmlElement
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	@XmlElement
	public String getDateColumn1() {
		return dateColumn1;
	}
	public void setDateColumn1(String dateColumn1) {
		this.dateColumn1 = dateColumn1;
	}
	@XmlElement
	public String getColumn2() {
		return column2;
	}
	public void setColumn2(String column2) {
		this.column2 = column2;
	}
	@XmlElement
	public String getColumn3() {
		return column3;
	}
	public void setColumn3(String column3) {
		this.column3 = column3;
	}
	@XmlElement
	public String getColumn4() {
		return column4;
	}
	public void setColumn4(String column4) {
		this.column4 = column4;
	}
	@XmlElement
	public String getColumn5() {
		return column5;
	}
	public void setColumn5(String column5) {
		this.column5 = column5;
	}
	@XmlElement
	public String getColumn6() {
		return column6;
	}
	public void setColumn6(String column6) {
		this.column6 = column6;
	}
	@XmlElement
	public String getColumn7() {
		return column7;
	}
	public void setColumn7(String column7) {
		this.column7 = column7;
	}
	@XmlElement
	public String getColumn8() {
		return column8;
	}
	public void setColumn8(String column8) {
		this.column8 = column8;
	}
	@XmlElement
	public String getColumn9() {
		return column9;
	}
	public void setColumn9(String column9) {
		this.column9 = column9;
	}
	@XmlElement
	public String getColumn10() {
		return column10;
	}
	public void setColumn10(String column10) {
		this.column10 = column10;
	}
	@Override
	public String toString() {
		return "Candidate [salutation=" + salutation + ", candidateName=" + candidateName + ", sid=" + sid
				+ ", courseName=" + courseName + ", guardianType=" + guardianType + ", fatherORHusbandName="
				+ fatherORHusbandName + ", sectorSkillCouncil=" + sectorSkillCouncil + ", dateOfIssuance="
				+ dateOfIssuance + ", level=" + level + ", aadhaarNumber=" + aadhaarNumber + ", sector=" + sector + "]";
	}

	

}
