package com.generate.certificate.genkartBE.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "candidates")
@XmlAccessorType (XmlAccessType.FIELD)
public class CandidateList {
	@XmlElement(name = "candidate")
	List<Candidate> candidates = new ArrayList<Candidate>();

	public List<Candidate> getCandidates() {
		return candidates;
	}

	public void setCandidates(List<Candidate> candidates) {
		this.candidates = candidates;
	}
	

}
