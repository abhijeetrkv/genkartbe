package com.generate.certificate.genkartBE.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Report {
	@JsonProperty
	private Long id;
	@JsonProperty
	private String generatedBy;
	@JsonProperty
	private String sid;
	@JsonProperty
	private Date generatedOn;
	@JsonProperty
	private String candidateName;
	@JsonProperty
	private String courseName;
	@JsonProperty
	private String level;
	@JsonProperty
	private Long templateID;
	@JsonProperty
	private String templateName;
	@JsonProperty
	private String trainingPartner;
	@JsonProperty
	private String batchId;
	
	
	public String getCandidateName() {
		return candidateName;
	}
	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getGeneratedBy() {
		return generatedBy;
	}
	public void setGeneratedBy(String generatedBy) {
		this.generatedBy = generatedBy;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public Date getGeneratedOn() {
		return generatedOn;
	}
	public void setGeneratedOn(Date generatedOn) {
		this.generatedOn = generatedOn;
	}
	public Long getTemplateID() {
		return templateID;
	}
	public void setTemplateID(Long templateID) {
		this.templateID = templateID;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTrainingPartner() {
		return trainingPartner;
	}
	public void setTrainingPartner(String trainingPartner) {
		this.trainingPartner = trainingPartner;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	@Override
	public String toString() {
		return "Report [id=" + id + ", generatedBy=" + generatedBy + ", sid=" + sid + ", generatedOn=" + generatedOn
				+ ", candidateName=" + candidateName + ", courseName=" + courseName + ", level=" + level
				+ ", templateID=" + templateID + ", templateName=" + templateName + "]";
	}

}
