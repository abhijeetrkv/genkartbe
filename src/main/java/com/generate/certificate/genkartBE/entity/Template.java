package com.generate.certificate.genkartBE.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Template {
	@JsonProperty
	private Long id;
	@JsonProperty
	private String templateName;
	@JsonProperty
	private Long userId;
	@JsonProperty
	private Date dateModified;
	@JsonProperty
	private Integer image_type;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Date getDateModified() {
		return dateModified;
	}
	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}
	public Integer getCertificateType() {
		return image_type;
	}
	public void setCertificateType(Integer certificateType) {
		this.image_type = certificateType;
	}
	@Override
	public String toString() {
		return "Template [id=" + id + ", templateName=" + templateName + ", userId=" + userId + ", dateModified="
				+ dateModified + ", certificateType=" + image_type + "]";
	}

}
