package com.generate.certificate.genkartBE.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDisplayModel {
	@JsonProperty
	private Long id;
	@JsonProperty
	private String email;
	@JsonProperty
	private String firstName;
	@JsonProperty
	private String lastName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Override
	public String toString() {
		return "UserDisplayModel [id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName
				+ "]";
	}
	

}
