package com.generate.certificate.genkartBE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.web.multipart.MultipartFile;

import com.generate.certificate.genkartBE.entity.Candidate;
import com.generate.certificate.genkartBE.entity.UserDisplayModel;

public class Utility {
	
	public static <T>String  convertObjectToXML(Object object, Class<T> classOb){
		StringWriter sw = new StringWriter();
		try {
			JAXBContext contextObj = JAXBContext.newInstance(classOb);
			Marshaller marshallerObj = contextObj.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-16");
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.marshal(object, sw);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return sw.toString();
	}

	@SuppressWarnings("unchecked")
	public static <T> T  convertXMLToObject(String xml, Class<T> classOb){
		Object returnOb = null;
		try {
			StringReader sr = new StringReader(xml);
			JAXBContext jaxbContext = JAXBContext.newInstance(classOb);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			returnOb  =  jaxbUnmarshaller.unmarshal(sr);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return (T) returnOb;
	}

	public static List<Candidate> getCandidatesFromFile(File file) {

		Workbook workbook = null;
		List<Candidate> candidates = new ArrayList<Candidate>();
		try {
				workbook = WorkbookFactory.create(new FileInputStream(file));
				System.out.println(workbook);
				Sheet sheet = workbook.getSheetAt(0);
				
				if(sheet.getSheetName().equals("Candidate Data")) {
					DataFormatter dataFormatter = new DataFormatter();

					int rowCount = 0;
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
					for (Row row: sheet) {
						//If its first row then skip it,as it has columns names, instead of values
						if(rowCount == 0) {
							rowCount++;
							continue;
						}
						//Reset column count to 0
						int columnCount = 0;
						// Make a new Candidate Object
						Candidate candidate = new Candidate();
						for(Cell cell: row) {
							String cellValue = dataFormatter.formatCellValue(cell);
							switch(columnCount) {
							case 0:
								candidate.setSalutation(cellValue);
								break;
							case 1:
								candidate.setCandidateName(cellValue);
								break;
							case 2:
								candidate.setSid(cellValue);
								break;
							case 3:
								candidate.setCourseName(cellValue);
								break;
							case 4:
								candidate.setGuardianType(cellValue);
								break;
							case 5:
								candidate.setFatherORHusbandName(cellValue);
								break;
							case 6:
								candidate.setSectorSkillCouncil(cellValue);
								break;
							case 7:
								try {
									String formatDate = sdf.format(new Date(cellValue));
									candidate.setDateOfIssuance(formatDate);
								} catch (Exception e) {
									e.printStackTrace();
								}
								break;
							case 8:
								candidate.setLevel(cellValue);
								break;
							case 9:
								candidate.setAadhaarNumber(cellValue);
								break;
							case 10:
								candidate.setSector(cellValue);
								break;
							case 11:
								candidate.setGrade(cellValue);
								break;
							case 12:
								try {
									String formatDate = sdf.format(new Date(cellValue));
									candidate.setDateOfStart(formatDate);
								} catch (Exception e) {
									e.printStackTrace();
								}
								break;
							
							case 13:
								candidate.setDistrict(cellValue);
								break;
							case 14:
								candidate.setState(cellValue);
								break;
							case 15:
								try {
									String formatDate = sdf.format(new Date(cellValue));
									candidate.setDateColumn1(formatDate);
								} catch (Exception e) {
									e.printStackTrace();
								}
								break;
							case 16:
								candidate.setColumn2(cellValue);
								break;
							case 17:
								candidate.setColumn3(cellValue);
								break;
							case 18:
								candidate.setColumn4(cellValue);
								break;
							case 19:
								candidate.setColumn5(cellValue);
								break;
							case 20:
								candidate.setColumn6(cellValue);
								break;
							case 21:
								candidate.setColumn7(cellValue);
								break;
							case 22:
								candidate.setColumn8(cellValue);
								break;
							case 23:
								candidate.setColumn9(cellValue);
								break;
							case 24:
								candidate.setColumn10(cellValue);
								break;
							}
							// System.out.print(cellValue + "\t");
							columnCount++;
						}
						// System.out.println();
						//Add candidate into candidates list
						candidates.add(candidate);
					}
				}else {
					throw new Exception("It seems you have uploaded a wrong file, Please try again with a valid file.");
				}
			return candidates;

		}catch(Exception e){
			e.printStackTrace();
		}
		return candidates;
	}

	public static String postData(String urlString, String json) {

		try {

			URL url = new URL(urlString);
			//System.out.println(urlString);
			//System.out.println(json);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			OutputStream os = conn.getOutputStream();
			os.write(json.getBytes());
			os.flush();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				output += output;
			}
			conn.disconnect();
			return output;

		} catch (Exception e) {
			e.printStackTrace();
		} 
		return "";
	}
	
}
