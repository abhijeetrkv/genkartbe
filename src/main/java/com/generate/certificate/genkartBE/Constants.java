package com.generate.certificate.genkartBE;

import java.util.Properties;

public class Constants {
	public static String APP_NAME;
	public static String API_KEY;
	public static String AUTH_API_URL;
	public static String API_SAVE_REPORT;
	public static String API_REGISTER;
	public static String API_TEMPLATE;
	public static String API_URL;
	public static String API_FILTER_REPORT;
	public static Properties prop = null;
	static{
		/*
		 * try { prop = new Properties();
		 * prop.load(Constants.class.getResourceAsStream("/app.properties")); } catch
		 * (Exception e) { e.printStackTrace(); }
		 * APP_NAME=prop!=null?prop.getProperty("application.name"):"";
		 * API_KEY=prop!=null?prop.getProperty("api.key"):"";
		 * AUTH_API_URL=prop!=null?prop.getProperty("api.auth.url"):"";
		 * API_SAVE_REPORT=prop!=null?prop.getProperty("api.report.save"):"";
		 */
		
		API_URL="http://103.190.243.5:8080/genkartBackEndAnuj";
		APP_NAME = "GenKart Powered by Urbanites Technologies";
		API_KEY =	"123456";
		AUTH_API_URL =	API_URL+"/user/get";
		API_SAVE_REPORT = API_URL+"/report/saveAll";
		API_REGISTER= API_URL+"/user/save";
		API_TEMPLATE= API_URL+"/template/get";
		API_FILTER_REPORT= API_URL+"/report/getFilteredReport";
	}
}
