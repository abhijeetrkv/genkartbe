package com.generate.certificate.genkartBE.basic.auth;


import java.util.Base64;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.generate.certificate.genkartBE.Constants;
import com.generate.certificate.genkartBE.controller.LoginController;
import com.generate.certificate.genkartBE.entity.User;
import com.generate.certificate.genkartBE.entity.UserDisplayModel;

//Controller
@CrossOrigin(origins= {"http://localhost:4200","http://103.190.242.205:8080"})
@RestController
public class BasicAuthenticationController {

	@GetMapping(path = "/basicauth")
	public User authenticateUser(@RequestHeader("Authorization") String authorizationString) throws Exception {

		String decodedAuthString = new String(Base64.getDecoder().decode(authorizationString));
		String email = decodedAuthString.split(":")[0];
		String pass= decodedAuthString.split(":")[1];
		try {
			String url = Constants.AUTH_API_URL+"?email="+email+"&password="+pass;
			ResponseEntity<User> responseEntity = new RestTemplate().getForEntity(
					url, User.class);
			if(responseEntity.getStatusCode() == HttpStatus.OK) {
				User user = responseEntity.getBody();
				user.setPassword("***********");
				
				if(email.equals(user.getEmail())) {
					//LoginController.loggedInuser = user;
					return user;
				}
			}else {
				throw new Exception("Bad Credentials");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Bad Credentials");
		}
		return new User();
		//throw new RuntimeException("Some Error has Happened! Contact Support at ***-***");
		
	}	
}
